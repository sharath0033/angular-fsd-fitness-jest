import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Subscription } from 'rxjs/internal/Subscription';

export class Contact {
  constructor(
    public firstname: string,
    public lastname: string,
    public phonenumber: number,
    public email: string,
    public message: string
  ) { }
}

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html'
})
export class ContactUsComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription = new Subscription();
  private onlyAlphabetsRegex: RegExp = /^[A-Za-z]+$/;
  @Output() contactdata = new EventEmitter<any>();
  isLoading: boolean = false;
  contactForm: FormGroup;

  constructor() { }

  ngOnInit() {
    this.contactForm = new FormGroup({
      firstname: new FormControl(null, [
        Validators.required,
        Validators.pattern(this.onlyAlphabetsRegex)
      ]),
      lastname: new FormControl(null, [
        Validators.required,
        Validators.pattern(this.onlyAlphabetsRegex)
      ]),
      email: new FormControl(null, [
        Validators.required,
        Validators.email
      ]),
      phonenumber: new FormControl(null, [
        Validators.required,
        Validators.min(1000000000),
        Validators.max(9999999999)
      ]),
      message: new FormControl(null, [Validators.required])
    });
  }

  onSubmit() {
    this.contactForm.markAllAsTouched();
    if (this.contactForm.valid) {
      this.contactForm.disable();
      this.isLoading = true;
      this.contactdata.emit({
        firstname: this.contactForm.get('firstname').value,
        lastname: this.contactForm.get('lastname').value,
        email: this.contactForm.get('email').value,
        phonenumber: this.contactForm.get('phonenumber').value,
        message: this.contactForm.get('message').value
      })
      this.contactForm.enable();
      this.isLoading = false;
    }
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
