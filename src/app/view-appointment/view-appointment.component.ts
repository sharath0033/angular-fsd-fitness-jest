import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';

import { UserService } from '../_services';

@Component({
  selector: 'app-view-appointment',
  templateUrl: './view-appointment.component.html'
})
export class ViewAppointmentComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription = new Subscription();
  isLoading: boolean = true;
  fitnessData: any;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.subscriptions.add(
    this.userService.getfitnessdata()
      .subscribe(response => this.fitnessData = response)
      .add(() => this.isLoading = false)
    )
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
