import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";

const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
@Injectable({ providedIn: 'root' })
export class UserService {
  public static BaseUrl = "http://localhost:6565/";

  constructor(private http: HttpClient) { }

  postfitnessdata(data) {
    return this.http.post(
      `${UserService.BaseUrl}allfriends`,
      data,
      { headers }
    )
  }

  getfitnessdata() {
    return this.http.get(
      `${UserService.BaseUrl}allfriends`,
      { headers }
    )
  }
}