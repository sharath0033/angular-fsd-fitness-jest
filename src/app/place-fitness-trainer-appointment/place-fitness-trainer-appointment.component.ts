import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Subscription } from 'rxjs/internal/Subscription';

import { UserService } from '../_services';

@Component({
  selector: 'app-place-fitness-trainer-appointment',
  templateUrl: './place-fitness-trainer-appointment.component.html'

})
export class PlaceFitnessTrainerAppointmentComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription = new Subscription();
  private onlyAlphabetsRegex: RegExp = /^[A-Za-z]+$/;
  isLoading: boolean = false;
  appointmentForm: FormGroup;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.appointmentForm = new FormGroup({
      firstname: new FormControl(null, [
        Validators.required,
        Validators.pattern(this.onlyAlphabetsRegex)
      ]),
      lastname: new FormControl(null, [
        Validators.required,
        Validators.pattern(this.onlyAlphabetsRegex)
      ]),
      email: new FormControl(null, [
        Validators.required,
        Validators.email
      ]),
      age: new FormControl(null, [
        Validators.required,
        Validators.min(18),
        Validators.max(60)
      ]),
      phonenumber: new FormControl(null, [
        Validators.required,
        Validators.min(1000000000),
        Validators.max(9999999999)
      ]),
      streetaddress: new FormControl(null, [Validators.required]),
      city: new FormControl(null, [Validators.required]),
      state: new FormControl(null, [Validators.required]),
      country: new FormControl(null, [Validators.required]),
      pincode: new FormControl(null, [
        Validators.required,
        Validators.min(100000),
        Validators.max(999999)
      ]),
      trainerpreference: new FormControl(null, [Validators.required]),
      physiotherapist: new FormControl(null, [Validators.required]),
      packages: new FormControl(null, [Validators.required]),
      inr: new FormControl(null, [Validators.required]),
      paisa: new FormControl(null, [Validators.required]),
    });
  }

  onSubmit() {
    this.appointmentForm.markAllAsTouched();
    if (this.appointmentForm.valid) {
      this.appointmentForm.disable();
      this.isLoading = true;
      this.subscriptions.add(
        this.userService.postfitnessdata({
          firstname: this.appointmentForm.get('firstname').value,
          lastname: this.appointmentForm.get('lastname').value,
          email: this.appointmentForm.get('email').value,
          age: this.appointmentForm.get('age').value,
          phonenumber: this.appointmentForm.get('phonenumber').value,
          streetaddress: this.appointmentForm.get('streetaddress').value,
          city: this.appointmentForm.get('city').value,
          state: this.appointmentForm.get('state').value,
          country: this.appointmentForm.get('country').value,
          pincode: this.appointmentForm.get('pincode').value,
          trainerpreference: this.appointmentForm.get('trainerpreference').value,
          physiotherapist: this.appointmentForm.get('physiotherapist').value,
          packages: this.appointmentForm.get('packages').value,
          inr: this.appointmentForm.get('inr').value,
          paisa: this.appointmentForm.get('paisa').value
        })
          .subscribe(() => alert('Appointment created'))
          .add(() => {
            this.appointmentForm.enable();
            this.isLoading = false;
          })
      )
    }
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
